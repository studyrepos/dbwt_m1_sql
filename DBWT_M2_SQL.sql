-- SQL stub, nutzen Sie dieses Snippet als Ausgangspunkt und erweitern und ändern Sie es so, 
-- dass es die Aufgaben aus Paket 2 löst
-- DROP statements
-- zuvor angelegte Tabellen löschen, niemals die ganze Datenbank löschen, nur Tabellen
-- mit DROP werden sowohl die Schemadefinitionen der Tabelle als auch die in ihr gespeicherten Daten

DROP TABLE IF EXISTS `ProduktEnthältZutat`;
DROP TABLE IF EXISTS `BestellungEnthältProdukt`;
DROP TABLE IF EXISTS `Bestellung`;
DROP TABLE IF EXISTS `Produkt`;
DROP TABLE IF EXISTS `Kategorie`;
DROP TABLE IF EXISTS `Bild`;
DROP TABLE IF EXISTS `Zutat`;
DROP TABLE IF EXISTS `Preis`;
DROP TABLE IF EXISTS `Mitarbeiter`;
DROP TABLE IF EXISTS `Gast`;
DROP TABLE IF EXISTS `Student`;
DROP TABLE IF EXISTS `FH-Angehörige`;
DROP TABLE IF EXISTS `FE-Nutzer`;

create table `FE-Nutzer`(
	Nr int unsigned auto_increment not null primary key,
	Vorname varchar(32) not null,
	Nachname varchar(32) not null,
	Loginname varchar(32) not null unique,
	EMail varchar(32) not null unique,
	`hash` char(24) not null,
	Salt char(32) not null,
	Stretch int unsigned not null,
	Algorithmus char(6) not null,
	Aktiv bool not null,
	Anlegedatum datetime not null default current_timestamp,
	LetzterLogin datetime
);

create table `Gast`(
	ID int unsigned not null primary key,
	Grund varchar(128) not null, 
	Ablauf datetime not null default current_timestamp,
	foreign key (ID) references `FE-Nutzer`(Nr)
		on delete cascade
		on update cascade
);

create table `FH-Angehörige`(
	ID int unsigned not null primary key,
	foreign key(ID) references `FE-Nutzer`(Nr)
		on delete cascade
		on update cascade
);

create table `Mitarbeiter`(
	ID int unsigned not null primary key,
	`MA-Nummer` int unsigned unique not null,
	Telefonnummer varchar(32),
	Büro varchar(32),
	foreign key (ID) references `FH-Angehörige`(ID)
		on delete cascade
		on update cascade
);

create table `Student`(
	ID int unsigned not null primary key,
	Matrikelnummer int unsigned unique not null,
	Studiengang varchar(64) not null,
	foreign key (ID) references `FH-Angehörige`(ID)
		on delete cascade
		on update cascade,
	check (Matrikelnummer between 9999 and 10000000)
);

CREATE TABLE `Bild`(
	ID INT unsigned AUTO_INCREMENT NOT NULL primary key,
	`Binärdaten` BLOB,
	AltText VARCHAR(60),
	Title VARCHAR(60),
	Unterschrift VARCHAR(80)
);

CREATE TABLE `Kategorie` (
	ID INT unsigned AUTO_INCREMENT not null primary key,
	Bezeichnung VARCHAR(100) not null,
	Oberkategorie INT unsigned default null,
	fkBildId int unsigned default null,
	CONSTRAINT `fk_kategorie_oberkategorie` FOREIGN KEY (Oberkategorie) REFERENCES `Kategorie`(ID)
		on delete cascade
		on update cascade,
	constraint `fk_kategorie_bild` foreign key (fkBildId) references `Bild`(ID)
		on delete cascade
		on update cascade
);

create table `Zutat`(
	ID int unsigned auto_increment not null primary key,
	Name varchar(32) not null,
	Beschreibung varchar(128) not null,
	Vegan bool,
	Vegetarisch bool,
	Glutenfrei bool,
	Bio bool
);

create table `Preis`(
	ID int unsigned auto_increment not null primary key,
	Mitarbeiterbetrag float not null,
	Studentbetrag float not null,
	Gastbetrat float not null
);

create table `Produkt`(
	ID int unsigned auto_increment not null primary key,
	Beschreibung varchar(128) not null,
	vegetarisch bool,
	vegan bool,
	fkPreisId int unsigned not null,
	fkBildId int unsigned not null,
	fkKategorieId int unsigned not null,
	constraint `fk_produkt_preis` foreign key(fkPreisId) references `Preis`(ID)
		on delete cascade
		on update cascade,
	constraint `fk_produkt_bild` foreign key(fkBildId) references `Bild`(ID)
		on delete cascade
		on update cascade,
	constraint `fk_produkt_kategorie` foreign key(fkKategorieId) references `Kategorie`(ID)
		on delete cascade
		on update cascade
);

create table `ProduktEnthältZutat`(
	ProduktId int unsigned not null,
	ZutatId int unsigned not null,
	primary key(ProduktId, ZutatId),
	foreign key (ProduktId) references `Produkt`(ID)
		on delete cascade
		on update cascade,
	foreign key (ZutatId) references `Zutat`(ID)
		on delete cascade
		on update cascade
);

create table `Bestellung`(
	ID int unsigned auto_increment not null primary key,
	fkID int unsigned not null,
	constraint `fk_nutzer_bestellung` foreign key (fkID) references `FE-Nutzer`(Nr)
		on delete cascade
		on update cascade
);

create table `BestellungEnthältProdukt`(
	BestellungId int unsigned not null,
	ProduktId int unsigned not null,
	Anzahl smallint unsigned not null,
	primary key(BestellungId, ProduktId),
	foreign key(BestellungId) references `Bestellung`(ID)
		on delete cascade
		on update cascade,
	foreign key(ProduktId) references `Produkt`(ID)
		on delete cascade
		on update cascade
);


--
--
-- INSERT statements
-- wenn alle Tabellen vollständig definiert sind, fügen Sie Beispieldaten in die Benutzertabellen ein (Aufgabe 2.4)
-- hier ein Insert-Beispiel für die im Stub definierte Tabelle Kategorie (wenn Sie das Schema ändern, kann es auch dazu kommen, dass Sie Änderungen in den INSERT Statements vornehmen müssen)

INSERT INTO `Kategorie` (ID, Bezeichnung, Oberkategorie) VALUES (1, 'Gebäck', NULL);

-- da ID automatisch vergeben wird, und die Oberkategorie per Default NULL ist, muss pro Eintrag nur das Attribut "Bezeichnung" angegeben werden
-- ausserdem können Sie in einem INSERT Statement mehrere Entitäten zum speichern angeben
INSERT INTO `Kategorie` (Bezeichnung) VALUES ('Vorspeisen'),('Desserts'),('Snacks'),('Mittagessen'); 

insert into `fe-nutzer` (Email, Loginname, Vorname, Nachname, Aktiv, `Hash`, Salt, Stretch, Algorithmus)
	values ('max.muster@ikea.de', 'maxi', 'Max', 'Muster', true, 741929282356176821677905, 33409845451960974818909031054791, 1, 'sha1');
insert into `FH-Angehörige` values (1);
insert into student values (1,749305,'Wirtschaftswissenschaften');

insert into `fe-nutzer` (Email, Loginname, Vorname, Nachname, Aktiv, `Hash`, Salt, Stretch, Algorithmus)
	values ('karl.heinz@fh.de', 'kalle', 'Karl', 'Heinz', false, 235741929216779052861768, 33409845451960974818909031054791, 1, 'sha1');
insert into `FH-Angehörige` values (2);
insert into student values (2,5736985,'Maschinenbau');

insert into `fe-nutzer` (Email, Loginname, Vorname, Nachname, Aktiv, `Hash`, Salt, Stretch, Algorithmus)
	values ('karl.keule@fh.de', 'keule', 'Karl', 'Keule', false, 292167235741979052861768, 33409845451960974818909031054791, 1, 'sha1');
insert into `FH-Angehörige` values (3);
insert into student values (3,6326985,'Maschinenbau');

insert into `fe-nutzer` (Email, Loginname, Vorname, Nachname, Aktiv, `Hash`, Salt, Stretch, Algorithmus)
	values ('anne.kranz@fh.de', 'anne', 'Anne', 'Kranz', true, 296779056176821282357419, 33409845451960974818909031054791, 1, 'sha1');
insert into `FH-Angehörige` values (4);
insert into mitarbeiter values (4, 12587, '0171/637555139', 'G157');

insert into `fe-nutzer` (Email, Loginname, Vorname, Nachname, Aktiv, `Hash`, Salt, Stretch, Algorithmus)
	values ('gunter.gast@tui.de', 'gunner', 'Gunter', 'Gast', true, 052817621677982357419296, 33409845451960974818909031054791, 1, 'sha1');
insert into gast (ID,Grund) values (5,'Bestes Schnitzel wo gibt...');

delete from `FE-Nutzer` where nr=2;

 